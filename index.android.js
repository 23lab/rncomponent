/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React from 'react';
import {
  AppRegistry,
} from 'react-native';
import SimpleApp from './js/index'
AppRegistry.registerComponent('SimpleApp', () => SimpleApp);

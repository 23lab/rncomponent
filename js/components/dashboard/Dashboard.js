import {
  Text,
  View,
  StyleSheet,
  TouchableHighlight,
  ScrollView,
  PixelRatio, Image
} from 'react-native'

import React, { Component } from 'react'
import Console from './plugins/console/Console'

export default class Dashboard extends Component {
  static propTypes = {
    onPressClose: React.PropTypes.func,
  }
  static registeredPlugins = []

  register (plugin) {
    Dashboard.registeredPlugins.push(plugin)
  }

  setup () {
    Dashboard.registeredPlugins.filter((plugin) => {
      return plugin && plugin.setup
    }).forEach((plugin) => {
      plugin.setup()
    })
  }

  render () {
    return (
      <View
        style={{
          marginTop: 18,
          flexDirection: 'column',
          flex: 1,
          justifyContent: 'flex-start'
        }}>
        {/*Tabs*/}
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'flex-start',
            backgroundColor: '#F3F3F3',
            height: 45,
          }}>
          {
            Dashboard.registeredPlugins.map((plugin, index) => {
              return (
                <View
                  key={index}
                  style={{
                    borderBottomWidth: 3,
                    borderBottomColor: '#3E82F7',
                    justifyContent: 'center',
                    paddingLeft: 5,
                    paddingRight: 5,
                  }}>
                  <Text
                    style={{
                      color: '#5A5A5A'
                    }}>{plugin.getName()}</Text>
                </View>
              )
            })
          }
          <View style={{flex: 1}}/>

          <TouchableHighlight onPress={this.props.onPressClose}>
            <View style={{
              justifyContent: 'center',
              paddingLeft: 20,
              paddingRight: 10,
              flex: 1,
            }}>
              <Image style={{width: 30, height: 30}} source={require('./images/close.png')}/>
            </View>
          </TouchableHighlight>
        </View>

        <View
          style={{
            justifyContent: 'flex-start',
            flex: 1,
          }}>
          {
            /* body */
            Dashboard.registeredPlugins.map((item, index) => {
              return (
                <View
                  key={index}
                  style={{
                    flex: 1,
                    alignSelf: 'stretch',
                  }}>
                  <Console />
                </View>
              )
            })
          }
        </View>
      </View>
    )
  }
}

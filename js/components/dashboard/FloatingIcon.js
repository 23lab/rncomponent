import {
  Text,
  View,
  StyleSheet,
  TouchableHighlight
} from 'react-native'

import React, { Component } from 'react'
import Console from './plugins/console/Console'
import Dashboard from './Dashboard'

export default class FloatingIcon extends Component {
  size = {
    iconSize: 50
  }

  state = {
    showDashboard: true
  }

  constructor (props) {
    super(props)
    this._toggleDashboard = this._toggleDashboard.bind(this)
  }

  _toggleDashboard () {
    this.setState({
      showDashboard: !this.state.showDashboard
    })
  }

  render () {
    if (!this.state.showDashboard) {
      return (
        <TouchableHighlight
          style={{
            position: 'absolute',
            right: 0,
            top: 100,
            width: this.size.iconSize,
            height: this.size.iconSize,
            backgroundColor: 'green'
          }}
          onPress={this._toggleDashboard}>
          <View>
          </View>
        </TouchableHighlight>
      )
    } else {
      return (
        <View
          style={{
            position: 'absolute',
            alignSelf: 'stretch',
            top: 0,
            bottom: 0,
            left: 0,
            right: 0,
            flex: 1,
            flexDirection: 'column'
          }}>
          <Dashboard onPressClose={this._toggleDashboard}/>
        </View>
      )
    }
  }

  // onPress={this._toggleDashboard}>
}

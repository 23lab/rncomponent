import {
  Text,
  View,
  StyleSheet,
  TouchableHighlight
} from 'react-native'
import React, { Component } from 'react'

import Dashboard from './components/dashboard/Dashboard'
import Console from './components/dashboard/plugins/console/Console'
import FloatingIcon from './components/dashboard/FloatingIcon'
import PopupStub from './components/popup/PopupStub'
import Toast from './components/popup/Toast'

export default class SimpleApp extends Component {
  constructor (props) {
    super(props)

    const dashboard = new Dashboard()
    dashboard.register(new Console())
    dashboard.setup()
  }
  render () {
    return (
      <View style={styles.container}>
        <Text style={styles.welcome}>
          Welcome to React Native!
        </Text>
        <TouchableHighlight onPress={this._onPress.bind(this)}>
          <Text style={styles.instructions}>
            Click Me
          </Text>
        </TouchableHighlight>
        <FloatingIcon />

        {/*<PopupStub ref={component => {*/}
          {/*this._popupStub = component*/}
          {/*Toast.init(this._popupStub)*/}
        {/*}} />*/}
      </View>
    )
  }

  _onPress () {
    for (let i = 0; i < 10; i++) {
      console.log('oonPress onPress onPress onPress onPress onPress onPress nPress ' + i)
      console.log({aaa: 11})
    }
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
})

